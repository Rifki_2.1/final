@extends('layout.master')

@section('title')
SignUp
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/kirim" method="post" >
@csrf
<label>Frist name :</label><br>
<input type="text" name="namdep"><br><br>
<label>Last Name :</label><br>
<input type="text" name="nambel"><br><br>
<label>Gender :</label><br>
<input type="radio" name="jk[]" value="1">Male<br>
<input type="radio" name="jk[]" value="2">Female<br>
<input type="radio" name="jk[]" value="3">Other<br><br>
<label>Nationality :</label><br>
<select name="negara" id="negara">
    <option value="indonesia">Indonesia</option>
    <option value="singapura">Singapore</option>
    <option value="malaysia">Malaysian</option>
    <option value="autralia">Australian</option>
</select><br><br>
<label>Language Spoken :</label><br>
<input type="checkbox" name="indo"> Indonesia<br>
<input type="checkbox" name="english"> english<br>
<input type="checkbox" name="Other"> Other<br><br>
<label>Bio :</label><br>
<textarea name="message" rows="10" cols="30"></textarea ><br><br>
<input type="submit" value="submit">

@endsection
