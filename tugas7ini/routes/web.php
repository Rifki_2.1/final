<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/cast',[CastController::class,'index'])->name('home');

Route::get('/cast/create',[CastController::class,'create'])->name('tambah');
Route::post('/cast',[CastController::class,'store'])->name('tambahAksi');

Route::get('/cast/{cast_id}',[CastController::class,'show'])->name('show');

Route::get('/cast/{cast_id}/edit',[CastController::class,'edit'])->name('edit');
Route::put('/cast/{cast_id}',[CastController::class,'update'])->name('editAksi');

Route::delete('/cast/{cast_id}',[CastController::class,'destroy'])->name('hapus');








Route::get('/game',[GameController::class,'index'])->name('homeGame');

Route::get('/game/create',[GameController::class,'create'])->name('tambahGame');
Route::post('/game',[GameController::class,'store'])->name('tambahAksiGame');

Route::get('/game/{game_id}',[GameController::class,'show'])->name('showGame');

Route::get('/game/{game_id}/edit',[GameController::class,'edit'])->name('editGame');
Route::put('/game/{game_id}',[GameController::class,'update'])->name('editAksiGame');

Route::delete('/game/{game_id}',[GameController::class,'destroy'])->name('hapusGame');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
