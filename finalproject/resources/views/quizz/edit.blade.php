<!doctype html>

<html lang="en">

    <head>

        <!-- Required meta tags -->

        <meta charset="utf-8">

        <meta
            name="viewport"
            content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->

        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
            integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn"
            crossorigin="anonymous">

        <title>Edit Data</title>

    </head>

    <body>

        <h2>Edit Data Game
            {{$data->id}}</h2>

        <form action="/game/{{$data->id}}" method="post">
            @csrf @method('PUT')
            <div class="form-group">
                <label>Nama Game</label><br>
                <input
                    type="text"
                    class="form-control"
                    id="name"
                    name="name"
                    value="{{$data->name}}"
                    placeholder="Masukkan Nama Game">
                @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Gameplay :</label><br>
                <textarea
                    name="gameplay"
                    id="gameplay"
                    class="form-control"
                    rows="10"
                    cols="30"
                    placeholder="Masukkan gameplay">{{$data->gameplay}}"</textarea>
                @error('gameplay')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Developer</label><br>
                <input
                    type="text"
                    class="form-control"
                    id="developer"
                    name="developer"
                    value="{{$data->developer}}"
                    placeholder="Masukkan Developer">
                @error('developer')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Tahun</label><br>
                <input
                    type="number"
                    class="form-control"
                    id="year"
                    name="year"
                    value="{{$data->year}}"
                    placeholder="Masukkan year">
                @error('year')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <input type="submit" class="btn btn-primary" value="submit">
        </form>

        <script
            src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
            crossorigin="anonymous"></script>

    </body>

</html>
