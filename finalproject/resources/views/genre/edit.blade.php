@extends('layout.master')

@section('title')
Edit Data
@endsection



@section('content')

<h2>Edit Data </h2>
        <form action="/genre/{{$data->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label>Genre Film</label><br>
                <input type="text" class="form-control" value="{{$data->nama}}" id="nama" name="nama" placeholder="Masukkan Genre FIlm">
                @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                @enderror
            </div>
            <input type="submit" class="btn btn-primary" value="submit">
@endsection
