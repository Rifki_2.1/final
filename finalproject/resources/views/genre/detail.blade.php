@extends('layout.master')


@section('title')
Genre FIlm
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@section('content')

<div class="row">
    @forelse ($data->film as $isi )
    <div class="col-4">
        <div class="card">
        <img class="card-img-top" style="height: 200px" src="{{asset('image/' . $isi->poster)}}" alt="Card image cap">
        <div class="card-body">
            <span class="badge badge-warning">{{$isi->genre->nama}}</span><br>
          <h5 class="card-title">{{$isi->judul}}</h5>
          <p class="card-text">{{Str::limit($isi->ringkasan, 20)}}</p>
            @auth
            <form action="/film/{{$isi->id}}" method="POST">
                @csrf
                <a href="/film/{{$isi->id}}" class="btn btn-info">Show</a>
                <a href="/film/{{$isi->id}}/edit" class="btn btn-warning">Edit</a>
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form>
            @endauth
            @guest
                <a href="/film/{{$isi->id}}" class="btn btn-info">Show</a>
            @endguest

        </div>
      </div>
    </div>

    @empty
      <h1>DATA KOSONG</h1>
    @endforelse
</div>
@endsection
