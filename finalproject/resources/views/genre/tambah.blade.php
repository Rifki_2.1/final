@extends('layout.master') @section('title') SignUp @endsection
@section('content')
<h1>Buat Data Genre Film Baru!</h1><br>
<h3>Masukkan Data</h3><br>
<form action="{{route('tambahAksiGenre')}}" method="post">
    @csrf
    <div class="form-group">
        <label>Genre Film</label><br>
        <input
            type="text"
            class="form-control"
            id="nama"
            name="nama"
            placeholder="Masukkan Genre Film">
        @error('nama')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <input type="submit" class="btn btn-primary" value="submit">
</form>



@endsection
