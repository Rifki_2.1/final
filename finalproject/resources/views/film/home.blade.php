@extends('layout.master')


@section('title')
Home
@endsection
@section('content')
@auth
<a href="{{route('tambahFilm')}}" class="btn btn-primary">+ Tambah Data</a>
@endauth


<div class="row">
    @forelse ($data as $isi )
    <div class="col-4">
        <div class="card">
        <img class="card-img-top" style="height: 300px "src="{{asset('image/' . $isi->poster)}}" alt="Card image cap">
        <div class="card-body">
            <span class="badge badge-warning">{{$isi->genre->nama}}</span><br>
          <h5 class="card-title">{{$isi->judul}}</h5>
          <p class="card-text">{{Str::limit($isi->ringkasan, 20)}}</p>
            @auth
            <form action="/film/{{$isi->id}}" method="POST">
                @csrf
                <a href="/film/{{$isi->id}}" class="btn btn-info">Show</a>
                <a href="/film/{{$isi->id}}/edit" class="btn btn-warning">Edit</a>
                @method('DELETE')
                <input type="submit" class="btn btn-danger my-1" value="Delete">
            </form>
            @endauth
            @guest
                <a href="/film/{{$isi->id}}" class="btn btn-info">Show</a>
            @endguest

        </div>
      </div>
    </div>

    @empty
      <h1>DATA KOSONG</h1>
    @endforelse
</div>


@endsection


