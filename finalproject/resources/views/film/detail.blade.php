@extends('layout.master')
@section('title')
Detail FIlm
@endsection
@section('content')
<div class="card">
    <img
        class="card-img-top"
        style="width: 250px"
        src="{{asset('image/' . $data->poster)}}"
        alt="Card image cap">
    <div class="card-body">
        <span class="badge badge-warning">{{$data->genre->nama}}</span><br>
        <h5 class="card-title">{{$data->judul}}</h5>
        <p class="card-text">{{$data->ringkasan}}</p>
        <a href="/" class="btn btn-primary">Kembali</a>
    </div>

    <h3>List Kritik</h3>
    @forelse ($data->kritik as $isi )
        <div class="media border">
            <h1>{{$isi->point}}</h1>
            <div class="media-bodi">
                <h5 class="mt-0">Name</h5>
                <p>{{$isi->content}}</p>
            </div>
        </div>
    @empty

    @endforelse
    <hr>
    @auth
        <form action="/kritik" method="POST">
        @csrf
        <input type="hidden" value="{{$data->id}}" name="film_id">
        <div class="form-group">
            <select name="point" class="form-control" id="">
                <option value="">--Pilih Point--</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
        </div>
        @error('point')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <textarea name="content" id="" class="form-control" cols="30" rows="10" placeholder="Masukkan Kritik"></textarea>
        </div>
        @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <input type="submit" class="btn btn-primary btn-sm" value="Kirim Kritik">
        </form>
    @endauth

</div>
@endsection
