@extends('layout.master') @section('title') SignUp @endsection
@section('content')
<h1>Tambah Film Baru!</h1>
<h3>Masukkan Data</h3>
<form action="{{route('tambahAksiFilm')}}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Judul</label><br>
        <input
            type="text"
            class="form-control"
            id="judul"
            name="judul"
            placeholder="Masukkan Judul FIlm">
        @error('judul')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Tahun</label><br>
        <input type="number" class="form-control" id="tahun" name="tahun" placeholder="Masukkan tahun">
        @error('tahun')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Ringkasan :</label><br>
        <textarea name="ringkasan" id="ringkasan" class="form-control" rows="10" cols="30" placeholder="Masukkan Ringkasan"></textarea>
        @error('ringkasan')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>poster</label><br>
        <input type="file" class="form-control" id="poster" name="poster" placeholder="Masukkan poster">
        @error('poster')
        <div class="alert alert-danger">
            {{ $message }}
        </div>
        @enderror
    </div>
    <div class="form-group">
        <label>Genre</label><br>
        <select class="form-control" name="genre_id" id="">
            <option value="">--PILIH GENRE--</option>
            @forelse ($genre as $item )
            <option value="{{$item->id}}">{{$item->nama}}</option>
            @empty
            <option value="">TIDAK ADA DATA</option>
            @endforelse
        </select>
    </div>
    <input type="submit" class="btn btn-primary" value="submit">
</form>



@endsection
