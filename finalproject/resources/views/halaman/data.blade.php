@extends('layout.master')


@section('title')
Show
@endsection

@push('script')
<script src="{{asset('admin/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush
@section('content')

<h2>Show No :{{$data->id}}</h2>
<h4>Nama :{{$data->nama}}</h4>
<h4>Umur :{{$data->umur}}</h4>
<p>Bio :{{$data->bio}}</p>
@endsection
