<?php

use App\Http\Controllers\CastController;
use App\Http\Controllers\FilmController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GameController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\KritikController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
Route::post('/kritik',[KritikController::class,'store'])->name('tambahAksiKritik');

Route::get('/cast',[CastController::class,'index'])->name('home');
Route::get('/cast/create',[CastController::class,'create'])->name('tambah');
Route::post('/cast',[CastController::class,'store'])->name('tambahAksi');
Route::get('/cast/{cast_id}',[CastController::class,'show'])->name('show');
Route::get('/cast/{cast_id}/edit',[CastController::class,'edit'])->name('edit');
Route::put('/cast/{cast_id}',[CastController::class,'update'])->name('editAksi');
Route::delete('/cast/{cast_id}',[CastController::class,'destroy'])->name('hapus');



    Route::get('/genre/create',[GenreController::class,'create'])->name('tambahGenre');
    Route::post('/genre',[GenreController::class,'store'])->name('tambahAksiGenre');

    Route::get('/genre/{genre_id}/edit',[GenreController::class,'edit'])->name('editGenre');
    Route::put('/genre/{genre_id}',[GenreController::class,'update'])->name('editAksiGenre');
    Route::delete('/genre/{genre_id}',[GenreController::class,'destroy'])->name('hapusGenre');
    Route::get('/film/create',[FilmController::class,'create'])->name('tambahFilm');
    Route::post('/film',[FilmController::class,'store'])->name('tambahAksiFilm');
    Route::get('/film/{film_id}/edit',[FilmController::class,'edit'])->name('editFilm');
    Route::put('/film/{film_id}',[FilmController::class,'update'])->name('editAksiFilm');
    Route::delete('/film/{film_id}',[FilmController::class,'destroy'])->name('hapusFilm');
});
Route::get('/',[FilmController::class,'index'])->name('homeFilm');
Route::get('/film/{film_id}',[FilmController::class,'show'])->name('showFilm');

Route::get('/genre',[GenreController::class,'index'])->name('homeGenre');
Route::get('/genre/{genre_id}',[GenreController::class,'show'])->name('showGenre');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


/*
quizz

Route::get('/game',[GameController::class,'index'])->name('homeGame');

Route::get('/game/create',[GameController::class,'create'])->name('tambahGame');
Route::post('/game',[GameController::class,'store'])->name('tambahAksiGame');

Route::get('/game/{game_id}',[GameController::class,'show'])->name('showGame');

Route::get('/game/{game_id}/edit',[GameController::class,'edit'])->name('editGame');
Route::put('/game/{game_id}',[GameController::class,'update'])->name('editAksiGame');

Route::delete('/game/{game_id}',[GameController::class,'destroy'])->name('hapusGame');
*/


