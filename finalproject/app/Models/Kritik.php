<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    use HasFactory;
    protected $table ='kritik';
    protected $fillable=['user_id','film_id','content','point'];


    public function film()
    {
      return $this->belongsToMany('Film::class');
    }
      public function user()
    {
      return $this->belongsToMany('Users::class');
    }
}
