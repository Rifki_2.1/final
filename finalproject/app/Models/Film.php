<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;
    protected $table = 'film';
    protected $fillable =['judul','tahun','ringkasan','poster','genre_id'];

    public function peran()
  {
   return $this->hasMany(Peran::class);
  }
    public function kritik()
  {
   return $this->hasMany(Kritik::class, 'film_id');
  }


    public function genre()
  {
    return $this->belongsTo(Genre::class);
  }
}
