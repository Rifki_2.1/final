<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;

use Illuminate\Http\Request;

class GenreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }
    
    public function index()
    {
        $data=Genre::all();
        return view('genre.home',compact('data'));
    }


    public function create()
    {
        return view('genre.tambah');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required|max:45'
        ]);
        Genre::create([
            'nama'=>$request->nama
        ]);
        return redirect('/genre');
    }


    public function show($id)
    {
        $data=Genre::find($id);
        return view('genre.detail',compact('data'));

    }


    public function edit( $id)
    {
        $data=Genre::find($id);
        return view('genre.edit',compact('data'));
    }


    public function update($id, Request $request,)
    {

        $this->validate($request,[
            'nama'=>'required|max:45'
        ]);

        $data = Genre::find($id);
        $data->nama = $request->nama;
        $data->update();
        return redirect('/genre');
    }


    public function destroy( $id)
    {
        $data = Genre::find($id);
        $data->delete();
        return redirect('/genre');
    }
}
