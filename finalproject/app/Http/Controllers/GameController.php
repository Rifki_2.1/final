
<?php

namespace App\Http\Controllers;

use App\Models\game;
use Illuminate\Http\Request;

class GameController extends Controller
{

    public function index()
    {
        $data = game::all();
        return view('quiz.indesx', compact('data'));
    }


    public function create()
    {
        return view('quiz.create');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
    		'name' => 'required|max:45',
    		'gameplay' => 'required',
            'developer' => 'required|max:45',
            'year' => 'required'

    	]);

        game::create([
    		'name' => $request->name,
    		'gameplay' => $request->gameplay,
            'developer' => $request->developer,
            'year' => $request->year
    	]);
        return redirect('/game');
    }


    public function show($id)
    {
        $data = game::find($id);
        return view('quiz.show',compact('data'));
    }


    public function edit($id)
    {
        $data = game::find($id);
        return view('quiz.edit',compact('data'));
    }


    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|max:45',
    		'gameplay' => 'required',
            'developer' => 'required|max:45',
            'year' => 'required'
        ]);

        $data = game::find($id);
        $data->name = $request->name;
        $data->gameplay = $request->gameplay;
        $data->developer = $request->developer;
        $data->year = $request->year;
        $data->update();
        return redirect('/game');
    }


    public function destroy($id)
    {
        $data = game::find($id);
        $data->delete();
        return redirect('/game');
    }
}
