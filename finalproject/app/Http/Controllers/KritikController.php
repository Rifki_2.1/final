<?php

namespace App\Http\Controllers;


use App\Models\Kritik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KritikController extends Controller
{
    public function store(Request $request)
    {
        $user_id = Auth::id();
        $request->validate([
            'content'=>'required',
            'point'=>'required'

        ]);
        $data = new Kritik;
        $data->user_id = $user_id;
        $data->film_id = $request->film_id;
        $data->content = $request->content;
        $data->point = $request->point;
        $data->save();
        return redirect('/film/'. $request->film_id);
    }
}
