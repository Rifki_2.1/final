<?php

namespace App\Http\Controllers;

use App\Models\Film;
use App\Models\Genre;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index','show');
    }

    public function index()
    {
        $data = film::all();
        return view('film.home', compact('data'));
    }


    public function create()
    {
        $genre = Genre::get();
        return view('film.tambah',['genre' => $genre]);
    }


    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required|max:45',
    		'ringkasan' => 'required',
            'poster' => 'required|mimes:jpg,png,jpeg',
            'genre_id' => 'required',
            'tahun' => 'required|numeric',

    	]);

        $filename = time().'.'.$request->poster->extension();
        $request->poster->move(public_path('image'), $filename);

        $data = new Film;
        $data->judul = $request->judul;
        $data->ringkasan = $request->ringkasan;
        $data->poster = $filename;
        $data->genre_id = $request->genre_id;
        $data->tahun = $request->tahun;
        $data->save();
        return redirect('/');
    }


    public function show($id)
    {
        $data =Film::find($id);
        return view('film.detail',compact('data'));
    }


    public function edit( $id)
    {
        $data = Film::find($id);
        return view('film.edit',compact('data'));
    }


    public function update(Request $request,$id)
    {
        $this->validate($request,[
    		'judul' => 'required|max:45',
    		'ringkasan' => 'required',
            'poster' => 'required|mimes:jpg,png,jpeg',
            'tahun' => 'required|max:4|numeric',
            'genre_id' => 'required|numeric'

    	]);

        $data = Film::find('id');
        $data->judul = $request->judul;
        $data->ringkasan = $request->ringkasan;
        $data->poster = $request->poster;
        $data->tahun = $request->tahun;
        $data->genre_id = $request->genre_id;
    }


    public function destroy( $id)
    {
        $data = Film::find($id);
        $data->delete();
        return redirect('/');
    }
}
